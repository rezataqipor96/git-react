import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle} from 'reactstrap';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
function RenderPromotionCard({item},props) {
    console.log('re',props.dish)
      return( 
                <Card>
                    <CardImg src={baseUrl+item.image} alt={item.name} />
                    <CardBody>
                    <CardTitle>{item.name}</CardTitle>
                    {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
                    <CardText>{item.description}</CardText>
                    </CardBody>
                </Card>
            );
}
    function RenderCard({item}) {          
            return( 
                <Card>
                    <CardImg src={baseUrl+item.image} alt={item.name} />
                    <CardBody>
                    <CardTitle>{item.name}</CardTitle>
                    {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
                    <CardText>{item.description}</CardText>
                    </CardBody>
                </Card>
            );
}
function Home(props) {
    if (props.leaderLoading) {
        return( <div className='container '>   <div className='row '>          
                <Loading />        
                <Loading />    
                <Loading /> 
                </div>      
                </div>
        );
    }
    else if (props.promoErrMess&&props.dishErrMess&&props.leaderErrMess) {
        return(
                <h4>{props.leaders.errMess}</h4>
        );
    }
    else 
        return(
            <div className="container">
                <div className="row align-items-start">
                    <div className="col-12 col-md m-1">
                    <RenderCard item={props.dish} />
                    </div>
                    <div className="col-12 col-md m-1">
                    <RenderPromotionCard item={props.promotion}  />
                    </div>
                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.leader} />
                    </div>
                </div>
            </div>
      );
    
  
}

export default Home;