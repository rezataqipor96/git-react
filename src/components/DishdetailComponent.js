import React, { Component } from 'react';
import { Card, CardBody, CardImg, CardText, Row, CardTitle, Breadcrumb, BreadcrumbItem, Modal, Button, ModalBody, ModalHeader, Label } from 'reactstrap'
import { Link } from 'react-router-dom'
import { Control, LocalForm,Errors } from 'react-redux-form'
import {Loading} from './LoadingComponent'
import { baseUrl } from '../shared/baseUrl';
function RenderDish({ dish,isLoading,errMess }) {
    if (isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{errMess}</h4>
                </div>
            </div>
        );
    }
    else if (dish != null)  {
        return (
            <Card>
                  <CardImg top src={baseUrl + dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>

        );
    } else {
        return <div></div>
    }
}
function RenderComments({ comments,addComment,dishId }) {
    if (comments != null) {
        return (
            <ul className="list-unstyled">
                <h4>Comments</h4>
                <div>{comments.map((comment) => {
                    return (
                        <div key={comment.id}>
                            <li>{comment.comment}</li>
                            <br />
                            <li>  -- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(comment.date)))}</li>
                            <br />
                        </div>);
                })}</div>
                <CommentForm dishId={dishId} addComment={addComment} />

            </ul>
        );
    }
    else {
        return <div></div>
    }
}
const required=(val)=>val&&val.length
const minLength=(len)=>(val)=> !(val)||(val.length>=len) 
const maxLength=(len)=>(val)=>!(val)||(val.length<=len)
    

export class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.toggleModal = this.toggleModal.bind(this)
        this.handleSubmit=this.handleSubmit.bind(this)
        this.state = { isModalOpen: false }
    }
    toggleModal() {
        this.setState({ isModalOpen: !this.state.isModalOpen })
    }
    handleSubmit(values){        
        this.toggleModal()
        this.props.addComment(this.props.dishId,values.selector,values.name,values.textarea)
    }
    render() {
        return (
            <>
                <Button onClick={this.toggleModal} outline color="secondary"><span className='fa fa-pencil fa-large'></span> Submit Comment</Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal} >
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                            <Row className='form-group'>
                                <Label htmlFor='selector'>
                                    <span className='mr-sm-2 ml-sm-2'>Rating</span>
                                </Label>
                                <Control.select className='custom-select mr-sm-2 ml-sm-2' model=".selector" name="selector">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Row>
                            <Row className='form-group'>
                                <Label htmlFor='name'>
                                    <span className='mr-sm-2 ml-sm-2'>Your Name</span>
                                </Label>
                                <Control.text model=".name" id="name" name="name" placeholder='Your Name' className='form-control ml-sm-2 mr-sm-2' validators={{required,maxLength:maxLength(15),minLength:minLength(3)}}/>
                                <Errors className='mr-sm-2 ml-sm-2 text-danger' show='touched' model='.name' messages={{required: 'Required', minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'}}/>
                            </Row>
                            <Row className='form-group'>
                                <Label className='ml-sm-2 mr-sm-2' htmlFor='textarea'>Comment</Label>
                                <Control.textarea rows="6" model='.textarea' className='form-control mr-sm-2 ml-sm-2' name='textarea' />
                            </Row>
                            <Row className='form-group'>
                            <Button className='mr-sm-2 ml-sm-2' color="primary">Submit</Button>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </>
        );
    }
}
const DishDetail = (props) => {
    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.dish.name}</h3>
                    <hr />
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish  dish={props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                <RenderComments comments={props.comments}
                addComment={props.addComment}
                    dishId={props.dish.id}
                    />
                </div>
            </div>
        </div>
    );
}


export default DishDetail;

